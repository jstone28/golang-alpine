FROM docker:dind

RUN apk add --no-cache \
  ca-certificates \
  bash \
  curl \
  git \
  go \
  make \
  musl-dev

# copy so that globally available
COPY --from=itential/argo /usr/bin/docker_setup /usr/bin/

# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH
